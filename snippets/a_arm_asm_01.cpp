// (C) 2024 A.Voß, a.voss@fh-aachen.de, cpp@codebasedlearning.dev

// temp. Stand !!!

int main() 
{
    int n = 0x11332244;
    int m = 0x22113322;
    int k = m+n;

    return 0;
}

/*
For AArch64 (64-bit ARM) processors, all instructions have the same fixed length of 32 bits (4 bytes).
This design is part of the RISC (Reduced Instruction Set Computing) architecture. Benefits:
  - Consistency: Every instruction is exactly 32 bits long, making decoding simpler and faster for the processor.
  - Predictability: Fixed-length instructions improve instruction fetch and pipeline efficiency because
    the CPU knows where the next instruction starts.
  - Alignment: Instructions are naturally aligned to 4-byte boundaries, simplifying memory access.

read all registers:
    register read

    x0...x28 General Purpose Registers
    fp (Frame Pointer), also known as x29 in ARM64;
       points to the base of the current stack frame, making it easier to access local variables and function arguments
    lr (Link Register), also known as x30 in ARM64;
       holds the return address of a subroutine (the address to return to after a function call);
       when a bl (Branch with Link) instruction is executed, the address of the next instruction is stored in lr
    sp (Stack Pointer)
       points to the top of the stack, which is used for temporary storage during function calls;
       the stack typically grows downward (toward lower memory addresses) on ARM
    pc (Program Counter)
       holds the address of the current instruction being executed;
       ARM processors use this register to determine the next instruction to fetch and execute
    cpsr (Current Program Status Register)
       holds information about the state of the processor and execution context;
       flags: neg, zero, carry, overflow; proc.mode bits; interrupt bits

read single register:
    register read sp

disassembly function main:
    di -n main -m -b

	sub	sp, sp, #0x10               sub <dest>, <src1>, <imm>           Subtracts the immediate value #0x10 from the stack pointer (sp), adjusting the stack downwards.
	mov	w0, #0                      mov <dest>, <imm>                   Moves the immediate value #0 into register w0.
	str	wzr, [sp, #0xc]             str <src>, [<base>, <offset>]       Stores the value of wzr (zero register, always contains 0) at the memory address [sp + #0xc].
    ---
int n = 0x11332244;
	mov	w8, #0x2244                 mov <dest>, <imm>                   Moves the immediate value #0x2244 into register w8.
	movk w8, #0x1133, lsl #16       movk <dest>, <imm>, lsl <shift>     Moves the immediate value #0x1133 into the upper 16 bits of w8 without altering the lower 16 bits.
	str	w8, [sp, #0x8]              str <src>, [<base>, <offset>]       Stores the value of w8 at the memory address [sp + #0x8].
    ---
int m = 0x22113322;
	mov	w8, #0x3322                 mov <dest>, <imm>
	movk w8, #0x2211, lsl #16       movk <dest>, <imm>, lsl <shift>
	str	w8, [sp, #0x4]              str <src>, [<base>, <offset>]
    ---
int k = m+n;
	ldr	w8, [sp, #0x4]              ldr <dest>, [<base>, <offset>]      Loads the value from memory address [sp + #0x4] into register w8.
	ldr	w9, [sp, #0x8]              ldr <dest>, [<base>, <offset>]      Loads the value from memory address [sp + #0x8] into register w9.
	add	w8, w8, w9                  add <dest>, <src1>, <src2>          Adds the values of w8 and w9 and stores the result in w8.
	str	w8, [sp]                    str <src>, [<base>]                 Stores the value of w8 at the memory address [sp].
    ---
	add	sp, sp, #0x10               add <dest>, <src1>, <imm>           Adds the immediate value #0x10 to the stack pointer (sp), adjusting the stack upwards.
	ret

read from sp-0x10:
    memory read -s4 -fx -c12 $sp-0x10

single instruction (step into):
    si

next instruction (step over):
    ni

register read sp

gdb / lldb commands:
    https://lldb.llvm.org/use/map.html

*/
