// (C) 2023 A.Voß, a.voss@fh-aachen.de, cpp@codebasedlearning.dev

bool g() { return true; }

// Im asm-Code sieht man, dass zu Beginn Platz für _alle_ lokalen Variablen
// geschaffen wird (SP-0x030), also nicht innerhalb der Funktion.
// Zusätzlich sieht man auch, dass immer in 0x10-Blöcken gerechnet wird.
void f() {
    int n1{0x1111},n2{0x2222},n3{0x3333},n4{0x4444};

    bool b = g();
    if (b) {    // lokale Variablen werden mit berücksichtigt, unabhängig davon, ob if-Block durchlaufen wird.
        int m12{0x1212},m23{0x2323},m34{0x3434};
    }

    int n5{0x5555};
}

int main() 
{
    f();

    return 0;
}

/* Kommentierung
 *
 * //https://lldb.llvm.org/use/map.html
 *
 * Disassembler main, f, g:
 * di -n main -m -b
 * di -n f -m -b
 */
