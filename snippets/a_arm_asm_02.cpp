// (C) 2024 A.Voß, a.voss@fh-aachen.de, cpp@codebasedlearning.dev

// temp. Stand !!!

int main()
{
    int n = 0x11332244;
    if (n>7) {
        int m = 5;
    } else {
        int k = 9;
    }

    return 0;
}

/*
disassembly function main:
    di -n main -m -b

main:
	sub	sp, sp, #16
	str	wzr, [sp, #12]
	mov	w8, #8772
	movk	w8, #4403, lsl #16
	str	w8, [sp, #8]
	ldr	w8, [sp, #8]
	subs	w8, w8, #7
	cset	w8, le                  set to 1 if le, otherwise to 0
	tbnz	w8, #0, LBB0_2          test bit 0, jump to
	b	LBB0_1                      jump to
LBB0_1:
	mov	w8, #5
	str	w8, [sp, #4]
	b	LBB0_3
LBB0_2:
	mov	w8, #9
	str	w8, [sp]
	b	LBB0_3
LBB0_3:
	mov	w0, #0
	add	sp, sp, #16
	ret
*/
