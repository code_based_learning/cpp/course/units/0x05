[© 2023, A.Voß, FH Aachen, codebasedlearning.dev](mailto:cpp@codebasedlearning.dev)

# Stack and ASM

---

## Push und Pop auf einem (rückwärts wachsendem) Stack (Prinzip, 16 Bit)

Die Variable (=Register) SP (stack pointer) bezeichnet eine Position im Speicher, 
an der Daten auf dem Stack abgelegt (push, SP wächst rückwärts) bzw. von dort 
wiedergeholt (pop, SP steigt) werden. 
Die Variable IP (instruction pointer) bezeichnet die nächste Position im Programm, 
an der ein Befehl ausgeführt wird. A ist eine beliebige Variable.

![Readme Image](./assets/stack_memory_v1.png)

---

## Lokale Variablen auf dem Stack (Prinzip 16 Bit)

Die Variable (=Register) BP bezeichnet eine Position im Speicher, zu der relativ 
lokale Variablen abgelegt werden. BP wird zu Beginn mit SP initialisiert und 
danach wächst SP (-=), damit künftige Calls die Daten nicht zerstören. 
Das muss am Ende wieder korrigiert werden (+=). 
Die Speicheradressen von n1 und n2 sind hier: 0x2304 und 0x2302.
Rückgabewerte werden z.B. in der Variablen A (=Register) zurückgegeben.

![Readme Image](./assets/stack_memory_v2.png)

---

## C++ vs. ASM

![Readme Image](./assets/stack_memory_v3.png)

---

End of `Stack and ASM`
