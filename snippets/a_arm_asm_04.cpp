// (C) 2024 A.Voß, a.voss@fh-aachen.de, cpp@codebasedlearning.dev

// temp. Stand !!!

int main() 
{
    int n = 0x11332244;
    int* pn{&n};
    int m = *pn;

    return 0;
}

/*

	sub	sp, sp, #32
	mov	w0, #0
	str	wzr, [sp, #28]
	add	x8, sp, #24                 here, x8 is sp+#24 (in fact, the adr of n)
	mov	w9, #8772
	movk	w9, #4403, lsl #16
	str	w9, [sp, #24]

	str	x8, [sp, #16]               store adr in sp+#16

	ldr	x8, [sp, #16]               load adr from sp+#16
	ldr	w8, [x8]                    load from [x8]
	str	w8, [sp, #12]
	add	sp, sp, #32

 */
