// (C) 2023 A.Voß, a.voss@fh-aachen.de, cpp@codebasedlearning.dev

int main() 
{
    int n{0x11223344};
    int* pn{nullptr};

    n = 0x22446688;
    pn = &n;
    *pn = 0x336699aa;
    return 0;
}

/* debug
 *
 * set breakpoint to 'int n' in main
 * run program with debug
 * change to 'LLDB' in debug window
 * di -n main -m -b
 * register read rip                -> show current command
 * me read -s4 -fx -c12 $rbp-0x10   -> memory contains nothing
 * si
 * me read -s4 -fx -c12 $rbp-0x10   -> memory contains n
 * si
 * me read -s4 -fx -c12 $rbp-0x10   -> memory contains n,pn
 * si
 * me read -s4 -fx -c12 $rbp-0x10   -> memory contains new n
 * si
 * register read rax                -> address of n (=bp-0x08)
 * si
 * me read -s4 -fx -c12 $rbp-0x10   -> memory contains n, adress of n
 * si
 * si
 * me read -s4 -fx -c12 $rbp-0x10   -> memory contains new n
 * ---
 *
 * //https://lldb.llvm.org/use/map.html
 *
 * Disassembler main, f, g:
 * di -n main -m -b
 * di -n f -m -b
 * di -n g -m -b
 * (-m is mixed mode, -b shows op-code bytes)
 *
 * Register read:
 * register read rip
 * register read edi
 * register read sp
 * register read rbp
 *
 * Memory read:
 * me read -s4 -fx -c12 $SP-0x10
 * me read -s4 -fx -c12 0x00007ffeefbd0a20
 * (-s4 size in bytes, -fx hex format, c12 number of items (3 lines))
 *
 * Steps:
 * si
 * (single step, step into)
 * ni
 * (next step)
 *
 */